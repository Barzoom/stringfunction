public class StringFun {
    public static void main(String[] args) {

        StringFun stringFun = new StringFun();

        String text = "Amy normally hated Monday mornings, but this year was different.";

        //Task #1 Кількість символів в тектсі;
        int countSymbols = text.length();
        System.out.println("Task #1: Count symbols in text = " + countSymbols);

        //Task #2 Кількість літер 'a';
        int countLetterA = stringFun.countSymbols(text,"a");
        System.out.println("Task #2: Count letter 'a' in text = " + countLetterA);

        //Task #3  Тескст з заміненими всіма літерами 'a' на 'A';
        String textUpA = stringFun.changeSymbolsToUpCase(text,"a");
        System.out.println("Task #3: Text with changed letters, a to A: " + textUpA);

        //Task #4 Всі слова у вигляді масиву
        String[] arrayWords = stringFun.stringToArray(text);
        System.out.println("Task #4:");
        for (String element: arrayWords) {
            System.out.println(element);
        }

        //Task #5 Частину тестку від першої букви 'a'  до останньої 'a'
        String shortText = text.substring(text.indexOf("a") + 1, text.lastIndexOf("a"));
        System.out.println("Task #5: Text from fist a to last a:" + shortText);

        //Task #6 Номери літер 'a' в тексті (індекси);
        System.out.println("Task #6: This text contain letter 'a' by indexes:" +
                stringFun.indexesPositionLetter(text, "a"));

        //Task #7 Стрічку задом наперед
        StringBuilder textReverse = new StringBuilder();
        textReverse.append(text);
        textReverse = textReverse.reverse();
        System.out.println("Task #7: Reverse text: " + textReverse);

        //Task #8 Вeсь текст малими літерами;
        System.out.println("Task #8: All text in lover case: " + text.toLowerCase());

        //Task #9 Весь текст великими літерами
        System.out.println("Task #9: All text in upper case: " + text.toUpperCase());
    }

    public int countSymbols(String text, String symbol) {
        int countSymbols = text.length() - text.replaceAll(symbol, "").length();
        return countSymbols;
    }

    public String changeSymbolsToUpCase(String text, String symbol) {
        return text.replaceAll(symbol,symbol.toUpperCase());
    }

    public String[] stringToArray(String text) {
        String[] arrayWords = text.split(" ");
        return arrayWords;
    }

    public String indexesPositionLetter(String text, String letter) {
        String indexes = "";
        int indexLetter;
        for (int position = 0; position <= text.lastIndexOf(letter); position++) {
            indexLetter = text.indexOf(letter, position);
            indexes += indexLetter + ", ";
            position = indexLetter;
        }
        return indexes;
    }
}
